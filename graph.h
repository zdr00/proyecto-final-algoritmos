#ifndef GRAPH_H
#define GRAPH_H

#include "linked_list.h"

//Nodo para Tweets
typedef struct TweetNode{
  char* id;
  char* content;
  struct TweetNode* nextTweet;
  char* userID;
}TweetNode;

// Nodo para usuarios
typedef struct UserNode
{
  char* id;                                     //El nombre de usuario será el identificador
  char* name;
  char* lastname;

  unsigned int counterID;                      //Es utilizado para crear un ID a un Tweet

  int tweets;                                  //Cantidad de Tweets del usuario
  struct TweetNode* lastTweet;
  struct LinkedList* following;
  struct LinkedList* followers;
}UserNode;

// Estructura que representa un grafo como un arreglo de listas adyacentes
// Tamano del arreglo será la variable V
typedef struct Graph
{
  LinkedList* users;
  LinkedList* tweets;
}Graph;

//Crea un nuevo nodo de usuario
UserNode* create_user_node(char* id, char* name, char* lastname)
{
  UserNode* newNode = (UserNode*) malloc(sizeof(UserNode));

  newNode->id = id;
  newNode->name = name;
  newNode->lastname = lastname;

  newNode->counterID = 0;
  newNode->tweets = 0;
  newNode->following = create_linked_list();
  newNode->followers = create_linked_list();
  newNode->lastTweet = NULL;
  return newNode;
}

//Crea un nuevo nodo de Tweet
TweetNode* create_tweet_node(char* id, char* content, char* userID){
  TweetNode* newNode = (TweetNode*) malloc(sizeof(TweetNode));

  newNode->id = id;
  newNode->content = content;
  newNode->nextTweet = NULL;
  newNode->userID = userID;
}

// Crea el grafo con las colleciones de Users y Tweets
Graph* createGraph()
{
    Graph* graph = (Graph*) malloc(sizeof(Graph));
    graph->users = (LinkedList*) malloc(sizeof(LinkedList));
    graph->tweets = (LinkedList*) malloc(sizeof(LinkedList));

    return graph;
}


// Agrega una arista al grafo de fuente a destino según sea un Tweet o un
// seguidor
void addEdgeFollow(UserNode* src, UserNode* dest){
  linked_list_add(src->following, dest);
}

void addEdgeFollower(UserNode* src, UserNode* dest){
  linked_list_add(src->followers, dest);
}

void addEdgeTweet(UserNode* src, TweetNode* dest)
{
  dest->nextTweet = src->lastTweet;
  src->lastTweet = dest;
}

//Remueve aristas especificas

int removeEdgeFollow(UserNode* src, char* id){
  return linked_list_remove(src->following, id);
}

int removeEdgeFollower(UserNode* src, char* id){
  return linked_list_remove(src->following, id);
}

int removeEdgeTweet(UserNode* src, char* id)
{
  if(src->tweets == 0){
    return 0;
  }
  TweetNode* prev = NULL;
  TweetNode* current = src->lastTweet;
  while(strcmp(current->id, id) != 0){
    prev = current;
    current =  current->nextTweet;
  }

  if(current == NULL){
    return 0;
  }

  if(prev == NULL){
    src->lastTweet= current->nextTweet;
  }else{
    prev->nextTweet = current->nextTweet;
    current->nextTweet = NULL;
  }

  src->tweets--;
}

/*----------------------------------------------------------------------------
 *  Métodos para crear/manejar la base de datos
 *--------------------------------------------------------------------------*/


//Funcion Find, se le pasa como collección el primer parametro y el id del objeto a buscar
UserNode* find_user(Graph* graph, char* id){
  UserNode* temp = (UserNode*) malloc(sizeof(UserNode));
  temp = (UserNode*) linked_list_get(graph->users, id);
  return temp;
}

TweetNode* find_tweet(Graph* graph, char* id){
  TweetNode* temp = (TweetNode*) malloc(sizeof(TweetNode));
  temp = (TweetNode*) linked_list_get(graph->tweets, id);

  return temp;
}
//-----------------------------------------------------//

//Inserta un nuevo usuario a la base de datos
void insert_user(Graph* graph, char* username, char* name, char* lastname){
  UserNode* temp = find_user(graph, username);
  if(temp != NULL){
    printf("\n ERROR: This username is already used. Please try a new one.\n");
    return;
  }

  UserNode* newNode = create_user_node(username, name, lastname);
  linked_list_add(graph->users, newNode);
}

//Inserta un nuevo tweet, es obligatorio especificar quien lo twitteo
void insert_tweet(Graph* graph, char* username, char* content){
  UserNode* user = find_user(graph, username);
  if(user == NULL){
    printf("\n ERROR: User doesn't exist.\n");
    return;
  }
  user->counterID++;
  user->tweets++;
  char* id = (char*) malloc(sizeof(char)*strlen(user->id));
  char str [sizeof(int)];

  strcpy(id, username);
  strcat(id, "/");
  sprintf(str, "%d", user->counterID);
  strcat(id, str);

  TweetNode* newTweet = create_tweet_node(id, content, user->id);
  addEdgeTweet(user, newTweet);
  linked_list_add(graph->tweets, newTweet);
}

//Crea el arista de "Follows" de un usuario a otro.
void follows(Graph* graph, char* srcName, char* destName){
  UserNode* src = find_user(graph, srcName);
  UserNode* dest = find_user(graph, destName);

  if(src == NULL || dest == NULL){
    printf("\n ERROR: User doesn't exist. \n");
  }

  addEdgeFollow(src, dest);
  addEdgeFollower(dest, src);
}

void print_tweets_user(Graph* graph, char* username)
{
  UserNode* user = find_user(graph, username);
  if(user == NULL){
    printf("\n ERROR: User doesn't exist. \n");
    return;
  }

  TweetNode* tweet = user->lastTweet;
  while(tweet != NULL){
    printf("\nUser: %s \nTweet: [%s]\n", username, tweet->content);
    tweet = tweet->nextTweet;
  }
  printf("\n" );
}

void print_all_tweets(Graph* graph){
  TweetNode* tweet;
  ListNode* node;
  node = (ListNode*) graph->tweets->head;

  while(node != NULL){
    tweet = (TweetNode*) node->key;
    printf("\nid:%s\nUser: %s \nTweet: [%s]\n", tweet->id,  tweet->userID, tweet->content);
    node = node->next;
  }
  printf("\n" );
}

//Imprime un tweet buscando en la colleccion de tweets
void print_tweet(Graph* graph, char* id){
  TweetNode* tweet = find_tweet(graph, id);
  if(tweet == NULL){
    printf("\n Tweet doesn't exist.\n");
  }else{
    printf("\nid:%s\nUser: %s \nTweet: [%s]\n", tweet->id,  tweet->userID, tweet->content);
  }
}

// Imprime la lista de adyacencia del grafo
void print_all_users(Graph* graph)
{
  ListNode* user;
  ListNode* following;
  UserNode* key;
  user = graph->users->head;

  while(user != NULL){
    key = (UserNode*) user->key;
    printf("\n[%s]", key->id);

    following = key->following->head;
    while(following != NULL){
      key = (UserNode*) following->key;
      printf("-> %s", key->id);
      following = following->next;
    }

    user = user->next;
  }
  printf("\n" );
}

void print_user(Graph* graph, char* username){
  UserNode* user = (UserNode*) find_user(graph, username);
  if(user == NULL){
    printf("\nERROR: User doesn't exist.\n");
    return;
  }

  ListNode* following = user->following->head;
  UserNode* key;

  printf("\nID: %s\nName: %s\n Lastname: %s\nTweets: %d\n Followers: %d\n Following: [%d]",
        user->id, user->name, user->lastname, user->tweets, user->followers->size, user->following->size);

  while(following != NULL){
    key = (UserNode*) following->key;
    printf("-> %s", key->id);
    following = following->next;
  }
}


//Da Unfollow a un usuario. Obligatorio fuente and destino
void remove_follow(Graph* graph, char* srcName, char* destName){
  UserNode* src = find_user(graph, srcName);
  UserNode* dest = find_user(graph, destName);

  if(src == NULL || dest == NULL){
    printf("\n ERROR: User doesn't exist. \n");
    return;
  }

  if(removeEdgeFollow(src, dest->id)){
    removeEdgeFollower(dest, src->id);

  }else{
    printf("\n ERROR: This relation doesn't exist. \n" );
    return;
  }
}

//Remueve un usuario de la base, pero no elimina sus tweets
void remove_user(Graph* graph, char* username){
  UserNode* temp = find_user(graph, username);
  if(temp == NULL){
    printf("\n ERROR: User doesn't exist. \n");
    return;
  }

  linked_list_remove(graph->users, username);
}

//Remueve el tweet de cierto usuario
void remove_tweet_user(Graph* graph, char* username, char* id){
    UserNode* user = find_user(graph, username);
    if(user == NULL){
      printf("\n ERROR: User doesn't exist. \n");
    }
    removeEdgeTweet(user, id);
    user->tweets--;
    linked_list_remove(graph->tweets, id);
}

//Remueve un tweet sin importar de que usuario sea
void remove_tweet(Graph* graph, char* id){
  TweetNode* tweet = find_tweet(graph, id);
  if(tweet == NULL){
    printf("\n ERROR: Tweet doesn't exist.\n");

  }

  UserNode* user = find_user(graph, tweet->userID);
  if(user != NULL){
    removeEdgeTweet(user, id);
    user->tweets--;
  }

  linked_list_remove(graph->tweets, tweet->id);
}

#endif
