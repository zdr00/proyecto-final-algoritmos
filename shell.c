#include "db.h"

Graph* graph;
/*
char* substring(char m[], int x, int z){
    if(x>z){
			int aux = x;
			x = z;
			z = aux;
		}

    int ax = z-x;
    char* ss = (char*) malloc(sizeof(char)*(z-x));
    int i = 0;

    for(i = x;i<z;i++){
			ss[i] = m[i];
		}

    return ss;
}

int indexOf(char s[], char m){
  int i;
  for (i = 0; i < strlen(s); i++) {
    if(s[i]==m){
      return i;
    }
  }
  return -1;
}

int lastIndexOf(char s[], char m){
  int i;
  for (i = strlen(s); i > 0; i--) {
    if(s[i]==m){
      return i;
    }
  }
  return -1;
}
*/

void parseCmd(char* cmd, char** params){
    int i;
    for(i = 0; i < 100; i++) {
        params[i] = strsep(&cmd, " ");
        if(params[i] == NULL) break;
    }
}


int tweetParser(char operacion[], char input[]){
  char* datos = (char*) malloc(strlen(input)*sizeof(char));
  strcpy(datos, input);

  if(strcmp(operacion,"insert") == 0){
    //printf("%s \n", datos);
    //here we try to add a new tweet
    char* user = strtok(datos, "{ :");
    char* content = strtok(NULL, ": }");
    //printf("\n%s\n%s\n",user, content);
    insert_tweet(graph, user, content);
  }

  if(strcmp(operacion,"find") == 0){
    if(strcmp(datos,")\n") == 0){
      //show all register
      printf("showing all the registers \n");
      print_all_tweets(graph);
    }else{
      //search for a specific tweet
      print_tweet(graph, datos);
    }
  }

  if(strcmp(operacion,"delete") == 0){
    remove_tweet(graph, datos);
  }

  return 2;
}

int userParser(char operacion[], char input[]){
  char* datos = (char*) malloc(strlen(input)*sizeof(char));
  strcpy(datos, input);
  if(strcmp(operacion,"insert") == 0){
    //printf("%s \n", datos);
    char* user = strtok(datos, ": ,");
    char* nombre = strtok(NULL, ": ,");
    char* apellido = strtok(NULL, ": )");
    //printf("\n%s\n%s\n%s\n",user, nombre, apellido );
    insert_user(graph, user, nombre, apellido);
    //insert a new user in the database
  }

  if(strcmp(operacion,"find") == 0){
    //printf("%s\n", datos );
    if(strcmp(datos,")\n") == 0){
      //show all registers
      print_all_users(graph);
    }else{
      //search for a specific user
      print_user(graph, datos);
    }
  }

  if(strcmp(operacion,"delete") == 0){
    //delete an specific user
    char* user = strtok(datos, "( )");
    printf("%s\n", user);
    remove_user(graph, user);
  }

  return 1;
}

int parseCommand(char str[]){

  int op;
  char* aux;

  aux = strtok (str,".(");
  aux = strtok (NULL,".(");

  if(strcmp(aux, "user") == 0){
    char* operacion = strtok (NULL, ".(");
    //strtok(NULL,"{");
    char* datos = strtok(NULL, "{}");

    op = userParser(operacion, datos);

  }


  if(strcmp(aux, "tweet") == 0){

    char* operacion = strtok (NULL, ".(");
    //strtok(NULL,"{");
    char* datos = strtok(NULL, "{}");

    op = tweetParser(operacion, datos);
  }

  return op;
}

void seed_db(Graph* graph){
  insert_user(graph,"chechare", "Cesar", "Robles");
  insert_user(graph,"mayruchis", "Mayra", "Paredes");
  insert_user(graph, "ypp", "Yessica", "Peña");
  insert_user(graph, "rosajmz", "Rosa", "Jimenez");
  insert_user(graph, "alf", "Alfredo", "Lopez");

  follows(graph, "chechare", "mayruchis");
  follows(graph, "chechare", "mayruchis");
  follows(graph, "chechare", "mayruchis");
  follows(graph, "rosajmz", "alf");
  follows(graph, "mayruchis", "rosajmz");
  follows(graph, "mayruchis", "chechare");

  insert_tweet(graph, "chechare", "estas?");
  insert_tweet(graph, "chechare", "como");
  insert_tweet(graph, "chechare", "hola,");
}

int main()
{
    char cmd[100];
    char* params[50];
    if((graph = createGraph()) != NULL){
      printf("\nBase de datos creada\n");
    }

    seed_db(graph);

    while(1) {
        char* username = getenv("USER");
        printf("%s@algoritmosDB> ", username);

        if(fgets(cmd, sizeof(cmd), stdin) == NULL) break;

        parseCmd(cmd, params);

        if(strcmp(params[0], "exit") == 0) break;

        if(strlen(params[0])<16){
          printf("Operación invalida\n");
          continue;
        }

        int a = parseCommand(params[0]);

        switch(a) {
           case 1  :
              printf("Operación concluida\n");
              break;

           case 2  :
              printf("Operación concluida\n");
              break;

           default :
              printf("Operación invalida \n");
              break;
        }
    }
    return 0;
}
