#ifndef HASH_TABLE_H_
#define HASH_TABLE_H_

struct list_s{
	int data;
	struct list_s *next;
};

typedef struct list_s list;

struct hash_table_s{
	int size;
	unsigned int n;
	int *random;
	struct list_s **table;
};

typedef struct hash_table_s hash_table;

//create hash table with size of 2^n
hash_table* init_hash_table(unsigned int n){
	hash_table *new_table=malloc(sizeof(hash_table));
	if(new_table==0){
		return 0;
	}
	int size=1<<n;
	new_table->table=(list**)calloc(size,sizeof(list*));
	if(new_table->table==0){
		return 0;
	}
	new_table->random=(int*)calloc(n,sizeof(int));
	if(new_table->random==0){
		return 0;
	}

	int i;
	srand((unsigned int)time(NULL));
	for(i=0;i<n;i++){
		new_table->random[i]=rand();
	}
	new_table->size=size;
	new_table->n=n;
	return new_table;
}

void insertHash(unsigned int hash, int key, hash_table *hash_table){
	if((hash_table->table[hash])==0){
		if((hash_table->table[hash]=malloc(sizeof(list)))!=0){
			hash_table->table[hash]->data=key;
			hash_table->table[hash]->next=0;
		}
	}
	else{
		list *cur;
		if((cur=malloc(sizeof(list)))!=0){
			cur=hash_table->table[hash];
			while((cur->next)!=0){
				cur=cur->next;
			}
			if((cur->next=malloc(sizeof(list)))!=0){
				cur->next->data=key;
				cur->next->next=0;
				cur=0;
				free(cur);
			}
		}
	}
}

int search(unsigned int hash, int key, hash_table *hash_table){
	int NOT_FOUND=INT_MIN;
	if(hash_table->table[hash]==0){
		printf("%d not found\n",key);
		return NOT_FOUND;
	}
	else{
		list *cur;
		if((cur=malloc(sizeof(list)))!=0){
			cur=hash_table->table[hash];
			while(cur!=0 && (cur->data)!=key){
				cur=cur->next;
			}
			if(cur!=0){
				return cur->data;
			}
			else{
				printf("%d not found\n", key);
				return NOT_FOUND;
			}
		}
		else{
			return NOT_FOUND;
		}
	}
}

void delete(unsigned int hash, int key, hash_table *table){
	if(table->table[hash]!=0){
		if(table->table[hash]->data==key){
			list *tmp=malloc(sizeof(list));
			if(tmp!=0){
				tmp=table->table[hash];
				table->table[hash]=table->table[hash]->next;
				free(tmp);
				tmp=0;
			}
		}
		else{
			list *cur=malloc(sizeof(list));
			list *prev=malloc(sizeof(list));
			if(cur!=0 && prev!=0){
				cur=table->table[hash]->next;
				prev=table->table[hash];
				while(cur!=0){
					if(cur->data==key){
						prev->next=cur->next;
						free(cur);
						cur=0;
						prev=0;
						free(prev);
					}
					else{
						cur=cur->next;
						prev=prev->next;
					}
				}
			}
		}
	}
}

unsigned int division_hash(int key, int size){
	return key%size;
}

unsigned int obtain_num(int *a,int n){
	int i;
	int c=1;
	int pow_2=1;
	unsigned int num=0;
	for(i=n-1;i>=0;i--){
		num+=(a[i])*(pow_2);
		pow_2=1<<c;
		c++;
	}
	return num;
}

int product_matrix_vector(int row, int vector){
	int i=row&vector;
	i=i-((i>>1)&0x55555555);
	i=(i&0x33333333)+((i>>2)&0x33333333);
	i=(((i+(i>>4))&0x0F0F0F0F)*0X01010101)>>24;
	return i&0x00000001;
}


unsigned int matrix_hash(int key, hash_table *table){

	int *col=(int*)calloc(table->n,sizeof(int));
	int r;
	int c=0;
	if(col!=0){
		while(c<table->n){
			col[c]=product_matrix_vector(table->random[c],key);
			c++;
		}
		return obtain_num(col,table->n);
	}
	else{
		exit(EXIT_FAILURE);
	}
}

void insert_matrix(int key, hash_table *table){
	unsigned int hash=matrix_hash(key,table);
	insertHash(hash,key,table);
}

int search_matrix(int key, hash_table *table){
	unsigned int hash=matrix_hash(key,table);
	return search(hash,key,table);
}

void delete_matrix(int key, hash_table *table){
	unsigned int hash=matrix_hash(key,table);
	delete(hash,key,table);
}

//n is the number such that 2^n=size
//hashing integers with size of 32 bits
unsigned int mult_hash(int key, int size, unsigned int n){
	double g_ratio=0.6180339887; //inverse of the golden ratio, recommendation by Knuth
	long word_size_2=4294967296; //2^32 is 2^w
	long S=(long)(g_ratio*word_size_2); // A2^w
	int shift=32-n; //numbers of bits to shift to obtain higher order bits of ro
	long mask=size-1;
	long r=S*key; //(A2^w)key=r1(2^w)+r0
	return (unsigned int)((r>>shift)&mask); //get the n most significant digits of ro
}
void insert_mult(int key, hash_table *table){
	unsigned int hash=mult_hash(key,table->size,table->n);
	insertHash(hash,key,table);
}

int search_mult(int key, hash_table *table){
	unsigned int hash=mult_hash(key,table->size,table->n);
	return search(hash,key,table);
}

void delete_mult(int key, hash_table *table){
	unsigned int hash=mult_hash(key,table->size,table->n);
	delete(hash,key,table);
}

void insert_division(int key, hash_table *table){
	unsigned int hash = division_hash(key,table->size);
	insertHash(hash,key,table);
}

int search_division(int key, hash_table *table){
	unsigned int hash = division_hash(key,table->size);
	return search(hash,key,table);
}

void delete_division(int key, hash_table *table){
	unsigned int hash = division_hash(key,table->size);
	delete(hash,key,table);
}

void print_table(hash_table *hash_table){
	int i;
	for(i=0;i<hash_table->size;i++){
		if(hash_table->table[i]==0){
			printf("%d ",i);
			printf("[X]\n");
		}
		else{
			list *cur=hash_table->table[i];
			printf("%d ", i);
			while(cur!=0){
				printf("[%d]->", cur->data);
				cur=cur->next;
			}
			printf("[X]\n");
		}
	}
}

void random_array(int A[], int size){
	int i;
	int r;
	srand((unsigned)time(NULL));
	for(i=0;i<size;i++){
		r=rand();
		A[i]=r;
	}
}

void identical_array(int A[], int size){
	int i;
	for(i=0;i<size;i++){
		A[i]=1;
	}
}

#endif
