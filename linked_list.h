#ifndef LINKED_LIST_H_
#define LINKED_LIST_H_

//Estructura de un nodo Generico
typedef struct Node{
  char* id;
}Node;

typedef struct ListNode{
  int vertex;
  void* key;
  struct ListNode* next;
}ListNode;

typedef struct LinkedList{
  int size;
  ListNode* head;
}LinkedList;

ListNode* newListNode(void* key){
  ListNode* node = (ListNode*) malloc(sizeof(ListNode)) ;
  node->key = key;
  node->next = NULL;
  return node;
}

LinkedList*  create_linked_list(){
  LinkedList* lili = (LinkedList*) malloc(sizeof(LinkedList));
  lili->head = NULL;
  lili->size = 0;
  return lili;
}

void linked_list_add(LinkedList* lili, void* key){
  ListNode* node = newListNode(key);
  //lili->rear->next =  node;
  node->next = lili->head;
  lili->head = node;
  lili->size++;
}

void* linked_list_get(LinkedList* lili, char* idx){
  if(lili->size == 0){
    return NULL;
  }

  ListNode* current = lili->head;
  struct Node* key = (Node*) current->key;
  while( current != NULL && strcmp(key->id, idx) != 0){
    printf("%s\n", key->id);
    current =  current->next;
    if(current != NULL){
      key = current->key;
    }
  }
  if(current == NULL){
    return NULL;
  }
  return current->key;
}

int linked_list_remove(LinkedList* lili, char* idx ){
  if(lili->size == 0){
    return 0;
  }
  ListNode* prev = NULL;
  ListNode* current = lili->head;
  struct Node* key = (Node*) current->key;
  while(current != NULL && strcmp(key->id, idx) != 0){
    prev = current;
    current =  current->next;
    key = current->key;
  }

  if(current == NULL){
    return 0;
  }

  if(prev == NULL){
    lili->head= lili->head->next;
  }else{
    prev->next = current->next;
    current->next = NULL;
  }

  lili->size--;
  return 1;
}

#endif
